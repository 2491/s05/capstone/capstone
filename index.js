//alert("B249!"")

// Customer class
	class Customer {
		constructor (name, email) {
			this.name = name;
			this.email = email;
			this.cart = new Cart (name);
			this.orders = [];
		}
		checkOut() {
			
		}
	}

//  Cart class

	class Cart {
		constructor (name) {
			this.user = name;
			this.contents = [];
			this.totalAmount = 0;
		}

		addToCart(product, quantity) {
			this.contents.push(
				{	product : product,
					quantity: quantity
				});

			this.computeTotal()
			return this;

		}

		showCartContents() {
			return this.contents;
		}

		updateProductQuantity(prodUpdate, newQuantity) {
			this.contents.forEach(i => {
				if (i.product.productName = prodUpdate) {
					i.quantity = newQuantity 
				}	
			})

			return this;

		}

		clearCartContents() {
			this.contents = [];
			return this;
		}

		computeTotal() {
			let tempTotal = 0
			this.contents.forEach(i => {
				tempTotal += (i.product.prodPrice*i.quantity)
			})
			this.totalAmount = tempTotal
			return this;

		}
	}

// Product class
	class Product {
		constructor (prodName, price) {
			this.productName = prodName;
			this.prodPrice = price;
			this.isActive = true;
		}

		archive() {
			this.isActive = false;
			return this;
		}

		updatePrice(newValue) {
			if (typeof newValue === "number") {
				this.prodPrice = newValue
			} else {console.log (`price must be number`)}
				return this;
		}
	}


//  Customer creation
	const john = new Customer ('John' , 'john@mail.com')

// Products
	const soap = new Product (`soap`, 9.99);
	const shampoo = new Product (`shampoo`, 10);
	const conditioner = new Product (`conditioner`, 13);


